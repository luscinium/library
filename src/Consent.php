<?php

declare(strict_types=1);

namespace Luscinium\Library;

use IABTcf\ConsentString;
use IABTcf\Decoder;
use JetBrains\PhpStorm\Pure;

class Consent
{
    private const NAME_COOKIE_CONSENT_AT = 'cookieConsentAt';
    private const NAME_COOKIE_CONSENT = 'cookieConsent';
    private const NAME_COOKIE_CONSENT_STATUS = 'cookieconsent_status2';
    private const NAME_COOKIE_FCCDCF = 'FCCDCF';

    private ?bool $consent = null;
    private ?\DateTimeInterface $consentAt = null;

    public function __construct()
    {
        $this->initializeFromCookie();
        $this->setCookies();
    }

    #[Pure]
    public function get(): ?bool
    {
        return $this->consent;
    }

    #[Pure]
    public function getConsentAt(): ?\DateTimeInterface
    {
        return $this->consent
            ? $this->consentAt
            : null;
    }

    private function initializeFromCookie(): void
    {
        $this->initializeConsentFromCookie();

        if ($this->consentAt === null) {
            if ($this->consent !== null) {
                $this->consentAt = new \DateTimeImmutable();
            }
            if (\array_key_exists(self::NAME_COOKIE_CONSENT_AT, $_COOKIE)) {
                try {
                    $this->consentAt = new \DateTimeImmutable($_COOKIE[self::NAME_COOKIE_CONSENT_AT]);
                } catch (\Exception) {
                }
            }
        }
    }

    private function initializeConsentFromCookie(): void
    {
        if ($this->setFromTCFCookie()) {
            return;
        }

        if (\array_key_exists(self::NAME_COOKIE_CONSENT, $_COOKIE)) {
            $this->consent = (bool) $_COOKIE[self::NAME_COOKIE_CONSENT];

            return;
        }

        if (\array_key_exists(self::NAME_COOKIE_CONSENT_STATUS, $_COOKIE)) {
            $this->consent = $_COOKIE[self::NAME_COOKIE_CONSENT_STATUS] === 'allow';
        }
    }

    private function setCookies(): void
    {
        if ($this->get() === null) {
            self::deleteCookie(self::NAME_COOKIE_CONSENT);
            self::deleteCookie(self::NAME_COOKIE_CONSENT_AT);
            self::deleteCookie(self::NAME_COOKIE_CONSENT_STATUS);

            return;
        }

        $value = $this->get() ? '1' : '0';
        $expires = time() + 60 * 60 * 24 * 30;

        setcookie(self::NAME_COOKIE_CONSENT, $value, $expires, '/');
        if ($consentAt = $this->getConsentAt()) {
            setcookie(self::NAME_COOKIE_CONSENT_AT, $consentAt->format('c'), $expires, '/');
        } else {
            self::deleteCookie(self::NAME_COOKIE_CONSENT_AT);
        }
    }

    private function setFromTCFCookie(): bool
    {
        if (\array_key_exists(self::NAME_COOKIE_FCCDCF, $_COOKIE)) {
            if ($cs = self::getConsentObject($_COOKIE[self::NAME_COOKIE_FCCDCF])) {
                $this->consent = $this->getConsentFromTCFCookie($cs);
                $this->consentAt = $cs->getLastUpdated();

                return true;
            }
        }

        return false;
    }

    private function getConsentFromTCFCookie(ConsentString $cs): bool
    {
        if ($cs->getPurposesConsent() !== []) {
            return true;
        }
        if ($cs->getVendorConsent() !== []) {
            return true;
        }
        if ($cs->getPubPurposesConsent() !== []) {
            return true;
        }

        return false;
    }

    private static function getConsentObject(string $cookie): ?ConsentString
    {
        try {
            $data = json_decode($cookie);
            if ($consentString = $data[3][0] ?? '') {
                return Decoder::decode($consentString);
            }
        } catch (\Exception) {
        }

        return null;
    }

    private static function deleteCookie(string $name): void
    {
        if (\array_key_exists($name, $_COOKIE)) {
            setcookie($name, '', 0, '/');
        }
    }
}
