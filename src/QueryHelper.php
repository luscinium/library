<?php

declare(strict_types=1);

namespace Luscinium\Library;

use JetBrains\PhpStorm\Pure;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;

class QueryHelper
{
    private function __construct()
    {
        throw new \LogicException("Can't touch this!");
    }

    /**
     * @psalm-suppress ImpureMethodCall
     */
    #[Pure]
    public static function getSearchTerm(Request $request, array $params): string
    {
        foreach ($params as $param) {
            if ($term = self::getQueryParam($request, $param)) {
                return trim($term);
            }
        }

        return '';
    }

    /**
     * @psalm-suppress ImpureMethodCall
     */
    #[Pure]
    public static function getQueryParam(Request $request, string $param): string
    {
        try {
            $param = (string) $request->request->get($param, $request->query->get($param, ''));
        } catch (\InvalidArgumentException|BadRequestException) {
            $param = '';
        }

        return trim($param);
    }

    #[Pure]
    public static function getBaseUri(): string
    {
        $baseUri = '';
        if (isset($_SERVER['HTTP_HOST'])) {
            $scheme = $_SERVER['REQUEST_SCHEME'] ?? '';
            if ($scheme === '' && isset($_SERVER['SERVER_PORT'])) {
                if ($_SERVER['SERVER_PORT'] === '80') {
                    $scheme = 'http';
                } elseif ($_SERVER['SERVER_PORT'] === '443') {
                    $scheme = 'https';
                }
            }
            if ($scheme) {
                $baseUri = $scheme.':';
            }
            $baseUri .= '//';

            $baseUri .= $_SERVER['HTTP_HOST'];
        }

        return $baseUri;
    }

    public static function getReferer(Request $request, array $params = []): string
    {
        $params[] = 'referer';
        foreach ($params as $param) {
            if ($targetPath = self::clean((string) $request->query->get($param))) {
                return $targetPath;
            }

            if ($targetPath = self::clean((string) $request->request->get($param))) {
                return $targetPath;
            }
        }

        return self::clean((string) $request->headers->get('referer'));
    }

    private static function clean(string $url): string
    {
        while (str_contains($url, '%25') || str_contains($url, '%2F')) {
            $url = urldecode($url);
        }

        if (($parts = parse_url($url)) && \array_key_exists('host', $parts)) {
            $url = $parts['path'] ?? '/';

            if (\array_key_exists('query', $parts)) {
                $url .= '?'.$parts['query'];
            }
        }

        return $url;
    }
}
