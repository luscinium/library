<?php

declare(strict_types=1);

namespace Luscinium\Library\RequestProperties\Exception;

class GeoipException extends \RuntimeException
{
}
