<?php

declare(strict_types=1);

namespace Luscinium\Library\RequestProperties;

use Jaybizzle\CrawlerDetect\CrawlerDetect;
use JetBrains\PhpStorm\Pure;
use Luscinium\Library\IpApi\Exception\IpApiException;
use Luscinium\Library\IpApi\IpApi;

class CrawlerHelper
{
    private const KNOWN_CRAWLERS = ['Googlebot', 'Bing', 'Apple Bot'];

    private function __construct()
    {
        throw new \LogicException("Can't touch this!");
    }

    /**
     * @psalm-suppress ImpureMethodCall
     */
    #[Pure]
    public static function isCrawler(string $userAgent, CrawlerDetect $crawlerDetect): bool
    {
        if ($userAgent && $crawlerDetect->isCrawler($userAgent)) {
            return true;
        }

        return false;
    }

    /**
     * @psalm-suppress ImpureMethodCall
     *
     * @throws IpApiException
     */
    #[Pure]
    public static function getCidr(string $ip, IpApi $ipApi): ?string
    {
        $ipApiResult = $ipApi->getResult($ip);

        return $ipApiResult?->getCidr();
    }

    /**
     * @psalm-suppress ImpureMethodCall
     *
     * @throws IpApiException
     */
    #[Pure]
    public static function isServer(string $ip, IpApi $ipApi): bool
    {
        $ipApiResult = $ipApi->getResult($ip);
        if ($ipApiResult && !$ipApiResult->isHome()) {
            return true;
        }

        return false;
    }

    /**
     * @psalm-suppress ImpureMethodCall
     */
    #[Pure]
    public static function isKnownCrawler(string $browserName): bool
    {
        return \in_array($browserName, self::KNOWN_CRAWLERS);
    }
}
