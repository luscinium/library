<?php

declare(strict_types=1);

namespace Luscinium\Library\RequestProperties;

use Illuminate\Contracts\Cache\Store;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use JetBrains\PhpStorm\Pure;
use Luscinium\Library\IpApi\Exception\IpApiException;
use Luscinium\Library\IpApi\IpApi;
use Luscinium\Library\RequestProperties\Exception\GeoipException;
use Luscinium\Library\RequestProperties\ValueObject\BrowserProperties;
use Luscinium\Library\RequestProperties\ValueObject\IpProperties;
use Luscinium\Library\RequestProperties\ValueObject\RequestProperties;
use Symfony\Component\HttpFoundation\Request;
use WhichBrowser\Parser;

readonly class RequestPropertiesBuilder
{
    private Parser $parser;
    private CrawlerDetect $crawlerDetect;

    #[Pure]
    public function __construct(
        private ?string $databaseFileName = null,
        private ?IpApi $ipApi = null,
        private ?Store $cache = null,
    ) {
        $this->parser = new Parser();
        $this->crawlerDetect = new CrawlerDetect();
    }

    /**
     * @throws GeoipException
     * @throws IpApiException
     */
    #[Pure]
    public function fromRequest(Request $request): RequestProperties
    {
        return $this->getRequestProperties($request);
    }

    #[Pure]
    public function fromRequestWithoutExceptions(Request $request): RequestProperties
    {
        return $this->getRequestProperties($request, true);
    }

    /**
     * @throws GeoipException
     * @throws IpApiException
     *
     * @psalm-suppress ImpureMethodCall
     */
    #[Pure]
    private function getRequestProperties(Request $request, bool $ignoreExceptions = false): RequestProperties
    {
        $ip = $request->getClientIp();
        $userAgent = $request->headers->get('User-Agent') ?? '';

        return $this->cache === null
            ? $this->buildRequestProperties($ip, $userAgent, $ignoreExceptions)
            : $this->getRequestPropertiesWithCache($ip, $userAgent, $ignoreExceptions, $this->cache);
    }

    /**
     * @throws GeoipException
     * @throws IpApiException
     *
     * @psalm-suppress ImpureMethodCall
     */
    #[Pure]
    private function getRequestPropertiesWithCache(?string $ip, string $userAgent, bool $ignoreExceptions, Store $cache): RequestProperties
    {
        $key = 'request_properties_'.($ip ?? '').'_'.$userAgent;
        $key = substr(preg_replace('/[^a-zA-Z0-9]/', '', $key), 0, 200);

        if ($requestProperties = $cache->get($key)) {
            return $requestProperties;
        }

        $requestProperties = $this->buildRequestProperties($ip, $userAgent, $ignoreExceptions);

        $cache->put($key, $requestProperties, 60 * 60);

        return $requestProperties;
    }

    /**
     * @throws GeoipException
     * @throws IpApiException
     *
     * @psalm-suppress ImpureFunctionCall
     * @psalm-suppress ImpureMethodCall
     */
    #[Pure]
    private function buildRequestProperties(?string $ip, string $userAgent, bool $ignoreExceptions): RequestProperties
    {
        $this->parser->analyse(getallheaders());

        if ($requestPropertiesForKnownCrawler = self::getRequestPropertiesForKnownCrawler($ip, $userAgent, $this->parser)) {
            return $requestPropertiesForKnownCrawler;
        }

        list($countryName, $cityName) = $this->getCountryAndCityName($ip, $ignoreExceptions);
        $isCrawler = CrawlerHelper::isCrawler($userAgent, $this->crawlerDetect);
        try {
            $ipProperties = IpPropertiesBuilder::buildIpProperties($ip, $this->ipApi);
        } catch (IpApiException $exception) {
            if (!$ignoreExceptions) {
                throw $exception;
            }
            $ipProperties = IpPropertiesBuilder::buildIpProperties($ip);
        }

        return new RequestProperties(
            $ipProperties,
            new BrowserProperties(
                $userAgent,
                $this->parser->browser->getName(),
                $this->parser->toString(),
                $isCrawler,
                false,
                OldBrowser::isOldBrowser($this->parser),
            ),
            $countryName,
            $cityName,
        );
    }

    /**
     * @psalm-suppress ImpureMethodCall
     */
    #[Pure]
    private static function getRequestPropertiesForKnownCrawler(?string $ip, string $userAgent, Parser $parser): ?RequestProperties
    {
        $browserName = $parser->browser->getName();
        if (CrawlerHelper::isKnownCrawler($browserName)) {
            $ipProperties = $ip === null
                ? null
                : new IpProperties(
                    $ip,
                    '',
                    true
                );

            return new RequestProperties(
                $ipProperties,
                new BrowserProperties(
                    $userAgent,
                    $browserName,
                    $parser->toString(),
                    true,
                    true,
                    false,
                ),
                null,
                null,
            );
        }

        return null;
    }

    /**
     * @throws GeoipException
     */
    #[Pure]
    private function getCountryAndCityName(?string $ip, bool $ignoreExceptions): array
    {
        $countryName = $cityName = null;
        if ($this->databaseFileName !== null) {
            try {
                if ($city = GeoHelper::getCity($ip, $this->databaseFileName)) {
                    $countryName = $city->country->isoCode;
                    $cityName = $city->city->name;
                }
            } catch (GeoipException $exception) {
                if (!$ignoreExceptions) {
                    throw $exception;
                }
            }
        }

        return [$countryName, $cityName];
    }
}
