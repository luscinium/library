<?php

declare(strict_types=1);

namespace Luscinium\Library\RequestProperties\ValueObject;

use JetBrains\PhpStorm\Pure;

readonly class BrowserProperties
{
    #[Pure]
    public function __construct(
        private string $userAgent,
        private string $browserName,
        private string $browserNiceName,
        private bool $isCrawler,
        private bool $isKnownCrawler,
        private bool $isOldBrowser,
    ) {
    }

    #[Pure]
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    #[Pure]
    public function getBrowserName(): string
    {
        return $this->browserName;
    }

    #[Pure]
    public function getBrowserNiceName(): string
    {
        return $this->browserNiceName;
    }

    #[Pure]
    public function isCrawler(): bool
    {
        return $this->isCrawler;
    }

    #[Pure]
    public function isKnownCrawler(): bool
    {
        return $this->isKnownCrawler;
    }

    #[Pure]
    public function isOldBrowser(): bool
    {
        return $this->isOldBrowser;
    }
}
