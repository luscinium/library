<?php

declare(strict_types=1);

namespace Luscinium\Library\RequestProperties\ValueObject;

use JetBrains\PhpStorm\Pure;

readonly class IpProperties
{
    #[Pure]
    public function __construct(
        private string $ip,
        private ?string $cidr,
        private bool $isServer,
    ) {
    }

    #[Pure]
    public function getIp(): string
    {
        return $this->ip;
    }

    #[Pure]
    public function getCidr(): ?string
    {
        return $this->cidr;
    }

    #[Pure]
    public function isServer(): bool
    {
        return $this->isServer;
    }
}
