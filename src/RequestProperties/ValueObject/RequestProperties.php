<?php

declare(strict_types=1);

namespace Luscinium\Library\RequestProperties\ValueObject;

use JetBrains\PhpStorm\Pure;

readonly class RequestProperties
{
    #[Pure]
    public function __construct(
        private ?IpProperties $ipProperties,
        private BrowserProperties $browserProperties,
        private ?string $countryName,
        private ?string $cityName,
    ) {
    }

    #[Pure]
    public function getIpProperties(): ?IpProperties
    {
        return $this->ipProperties;
    }

    #[Pure]
    public function getBrowserProperties(): BrowserProperties
    {
        return $this->browserProperties;
    }

    #[Pure]
    public function getCountryName(): ?string
    {
        return $this->countryName;
    }

    #[Pure]
    public function getCityName(): ?string
    {
        return $this->cityName;
    }
}
