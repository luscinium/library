<?php

declare(strict_types=1);

namespace Luscinium\Library\RequestProperties;

use JetBrains\PhpStorm\Pure;
use WhichBrowser\Parser;

readonly class OldBrowser
{
    private const MIN_BROWSER_VERSIONS = [
        'Chrome' => 109, // https://en.wikipedia.org/wiki/Template:Google_Chrome_release_compatibility & https://support.google.com/chrome/a/answer/7100626?hl=en
        'Firefox' => 115, // https://en.wikipedia.org/wiki/Firefox_version_history#Release_compatibility
    ];

    /**
     * @psalm-suppress InternalProperty
     * @psalm-suppress ImpureMethodCall
     */
    #[Pure]
    public static function isOldBrowser(Parser $parser): bool
    {
        $browserName = $parser->browser->getName();

        if (!\array_key_exists($browserName, self::MIN_BROWSER_VERSIONS)) {
            return false;
        }

        return $parser->browser->version->is('<', self::MIN_BROWSER_VERSIONS[$browserName]);
    }
}
