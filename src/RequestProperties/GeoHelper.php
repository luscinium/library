<?php

declare(strict_types=1);

namespace Luscinium\Library\RequestProperties;

use GeoIp2\Database\Reader;
use GeoIp2\Model\City;
use JetBrains\PhpStorm\Pure;
use Luscinium\Library\RequestProperties\Exception\GeoipException;

class GeoHelper
{
    private function __construct()
    {
        throw new \LogicException("Can't touch this!");
    }

    /**
     * @throws GeoipException
     *
     * @psalm-suppress ImpureMethodCall
     */
    #[Pure]
    public static function getCity(?string $ip, string $databaseFileName): ?City
    {
        if ($ip === null) {
            return null;
        }

        try {
            $reader = new Reader($databaseFileName);
        } catch (\Exception $e) {
            throw new GeoipException($e->getMessage(), previous: $e);
        }

        try {
            $city = $reader->city($ip);
        } catch (\Exception $e) {
            throw new GeoipException($e->getMessage(), previous: $e);
        }

        self::assertHomeIp($city);

        return $city;
    }

    /**
     * @throws GeoipException
     */
    private static function assertHomeIp(City $city): void
    {
        $traits = $city->traits;
        if ($traits->isAnonymous) {
            throw new GeoipException('IP is anonymous');
        }
        if ($traits->isAnonymousVpn) {
            throw new GeoipException('IP is anonymous VPN');
        }
        if ($traits->isAnycast) {
            throw new GeoipException('IP is anycast');
        }
        if ($traits->isHostingProvider) {
            throw new GeoipException('IP is hosting provider');
        }
        if ($traits->isLegitimateProxy) {
            throw new GeoipException('IP is legitimate proxy');
        }
        if ($traits->isPublicProxy) {
            throw new GeoipException('IP is public proxy');
        }
        if ($traits->isResidentialProxy) {
            throw new GeoipException('IP is residential proxy');
        }
        if ($traits->isTorExitNode) {
            throw new GeoipException('IP is tor exit node');
        }
    }
}
