<?php

declare(strict_types=1);

namespace Luscinium\Library\RequestProperties;

use JetBrains\PhpStorm\Pure;
use Luscinium\Library\IpApi\Exception\IpApiException;
use Luscinium\Library\IpApi\IpApi;
use Luscinium\Library\RequestProperties\ValueObject\IpProperties;

readonly class IpPropertiesBuilder
{
    /**
     * @throws IpApiException
     */
    public static function buildIpProperties(?string $ip, ?IpApi $ipApi = null): ?IpProperties
    {
        if ($ip === null) {
            return null;
        }

        return new IpProperties(
            $ip,
            self::getServerCidr($ip, $ipApi),
            self::isServer($ip, $ipApi),
        );
    }

    /**
     * @throws IpApiException
     */
    #[Pure]
    public static function getServerCidr(?string $ip, ?IpApi $ipApi): ?string
    {
        if ($ip === null || $ipApi === null) {
            return null;
        }

        return CrawlerHelper::getCidr($ip, $ipApi);
    }

    /**
     * @throws IpApiException
     */
    #[Pure]
    public static function isServer(?string $ip, ?IpApi $ipApi): bool
    {
        if ($ip === null || $ipApi === null) {
            return false;
        }

        return CrawlerHelper::isServer($ip, $ipApi);
    }
}
