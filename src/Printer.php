<?php

declare(strict_types=1);

namespace Luscinium\Library;

use Symfony\Component\Console\Output\OutputInterface;

class Printer
{
    private function __construct()
    {
        throw new \LogicException("Can't touch this!");
    }

    public static function error(?OutputInterface $output, string $errorMessage): void
    {
        if ($output === null) {
            return;
        }
        self::print($output, '<error>'.$errorMessage.'</error>', OutputInterface::VERBOSITY_QUIET);
    }

    public static function warn(?OutputInterface $output, string $warnMessage): void
    {
        if ($output === null) {
            return;
        }
        self::print($output, '<fg=red>'.$warnMessage.'</>', OutputInterface::VERBOSITY_NORMAL);
    }

    public static function notice(?OutputInterface $output, string $noticeMessage): void
    {
        if ($output === null) {
            return;
        }
        self::print($output, '<fg=red>'.$noticeMessage.'</>', OutputInterface::VERBOSITY_VERBOSE);
    }

    public static function info(?OutputInterface $output, string $infoMessage): void
    {
        if ($output === null) {
            return;
        }
        self::print($output, '<info>'.$infoMessage.'</info>', OutputInterface::VERBOSITY_NORMAL);
    }

    public static function debug(?OutputInterface $output, string $debugMessage): void
    {
        if ($output === null) {
            return;
        }
        self::print($output, $debugMessage, OutputInterface::VERBOSITY_VERBOSE);
    }

    private static function print(OutputInterface $output, string $message, int $verbosity): void
    {
        $output->writeln(
            \sprintf(
                '<comment>[%s]</comment> %s <comment>[%.1fMB]</comment>',
                (new \DateTimeImmutable())->format('H:i:s.v'),
                $message,
                memory_get_usage() / (1024 * 1024)
            ),
            $verbosity
        );
    }
}
