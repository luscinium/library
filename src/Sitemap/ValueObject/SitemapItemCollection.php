<?php

declare(strict_types=1);

namespace Luscinium\Library\Sitemap\ValueObject;

use JetBrains\PhpStorm\Pure;

class SitemapItemCollection
{
    /**
     * @var SitemapItem[]
     */
    private array $items = [];
    private ?int $lastMod = null;

    public function __construct(
        private readonly ?string $fileName = null,
    ) {
    }

    public function addItem(SitemapItem $item): void
    {
        $this->items[] = $item;
        $this->lastMod = $this->lastMod === null
            ? $item->getLastMod()
            : max($item->getLastMod(), $this->lastMod);
    }

    /**
     * @return SitemapItem[]
     */
    #[Pure]
    public function getItems(): array
    {
        return $this->items;
    }

    #[Pure]
    public function getLastMod(): ?int
    {
        return $this->lastMod;
    }

    #[Pure]
    public function getFileName(): ?string
    {
        return $this->fileName;
    }
}
