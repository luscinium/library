<?php

declare(strict_types=1);

namespace Luscinium\Library\Sitemap\ValueObject;

use JetBrains\PhpStorm\Pure;

readonly class SitemapIndexItem
{
    public function __construct(
        private string $loc,
        private int $lastmod,
    ) {
    }

    #[Pure]
    public function getLoc(): string
    {
        return $this->loc;
    }

    #[Pure]
    public function getLastmod(): int
    {
        return $this->lastmod;
    }

    #[Pure]
    public function getLastmodAsString(): string
    {
        return (new \DateTimeImmutable())->setTimestamp($this->lastmod)->format('c');
    }

    #[Pure]
    public function asXml(): string
    {
        $xml = "\t<sitemap>\n";
        $xml .= "\t\t<loc>".$this->getLoc()."</loc>\n";
        $xml .= "\t\t<lastmod>".$this->getLastmodAsString()."</lastmod>\n";
        $xml .= "\t</sitemap>\n";

        return $xml;
    }

    public static function getSitemapTemplate(): string
    {
        return <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
%s
</sitemapindex>
XML;
    }
}
