<?php

declare(strict_types=1);

namespace Luscinium\Library\Sitemap\ValueObject;

use JetBrains\PhpStorm\Pure;

readonly class SitemapItem
{
    public function __construct(
        private string $loc,
        private int $lastMod,
        private string $changeFreq,
        private float $priority,
    ) {
    }

    #[Pure]
    public function getLoc(): string
    {
        return $this->loc;
    }

    #[Pure]
    public function getLastMod(): int
    {
        return $this->lastMod;
    }

    #[Pure]
    public function getLastModAsString(): string
    {
        return (new \DateTimeImmutable())->setTimestamp($this->lastMod)->format('c');
    }

    #[Pure]
    public function getChangeFreq(): string
    {
        return $this->changeFreq;
    }

    #[Pure]
    public function getPriority(): float
    {
        return $this->priority;
    }

    #[Pure]
    public function asXml(string $baseUri = ''): string
    {
        $xml = "\t<url>\n";
        $xml .= "\t\t<loc>".$baseUri.$this->getLoc()."</loc>\n";
        $xml .= "\t\t<lastmod>".$this->getLastModAsString()."</lastmod>\n";
        $xml .= "\t\t<changefreq>".$this->getChangeFreq()."</changefreq>\n";
        $xml .= "\t\t<priority>".$this->getPriority()."</priority>\n";
        $xml .= "\t</url>\n";

        return $xml;
    }

    #[Pure]
    public static function getSitemapTemplate(): string
    {
        return <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
%s
</urlset>
XML;
    }
}
