<?php

declare(strict_types=1);

namespace Luscinium\Library\Sitemap;

use Luscinium\Library\Sitemap\Exception\SitemapFileException;
use Luscinium\Library\Sitemap\ValueObject\SitemapIndexItem;
use Luscinium\Library\Sitemap\ValueObject\SitemapItem;
use Luscinium\Library\Sitemap\ValueObject\SitemapItemCollection;

class SitemapWriter
{
    public const MAX_ITEMS_PER_FILE = 40000;

    private function __construct()
    {
        throw new \LogicException("Can't touch this!");
    }

    /**
     * @throws SitemapFileException
     *
     * @return SitemapIndexItem[]
     *
     * @psalm-suppress RedundantCondition
     */
    public static function writeXML(
        string $baseUri,
        string $dir,
        string $fileName,
        SitemapItemCollection $collection,
    ): array {
        $lastUpdate = $collection->getLastMod() ?? time();
        $items = $collection->getItems();
        if (\count($items) === 0) {
            return [];
        }

        if (\count($items) <= self::MAX_ITEMS_PER_FILE) {
            return [self::writeRealXML($baseUri, $dir, $fileName, $lastUpdate, $items)];
        }

        $indexes = [];
        $fileNumber = 1;
        while (\count($items)) {
            $itemsPart = array_splice($items, 0, self::MAX_ITEMS_PER_FILE);

            $indexes[] = self::writeRealXML($baseUri, $dir, $fileName.($fileNumber > 1 ? '_'.$fileNumber : ''), $lastUpdate, $itemsPart);
            ++$fileNumber;
        }

        return $indexes;
    }

    /**
     * @param SitemapIndexItem[] $indexes
     *
     * @throws SitemapFileException
     */
    public static function writeIndex(
        string $dir,
        string $fileName,
        array $indexes,
    ): void {
        if (!$xmlArchive = gzopen($dir.$fileName.'.xml.gz', 'wb9')) {
            throw new SitemapFileException(\sprintf('Error opening outputfile [%s%s.xml.gz]', $dir, $fileName));
        }

        $urlXml = '';
        foreach ($indexes as $index) {
            $urlXml .= $index->asXml();
        }

        $xml = \sprintf(SitemapIndexItem::getSitemapTemplate(), $urlXml);

        gzwrite($xmlArchive, $xml);
        gzclose($xmlArchive);
    }

    /**
     * @param SitemapItemCollection[] $sitemapItemCollections
     *
     * @throws SitemapFileException
     */
    public static function writeCollectionsAndIndex(
        string $baseUri,
        string $uriPrefix,
        string $dir,
        array $sitemapItemCollections,
        string $indexFileName,
    ): void {
        $files = [];
        foreach ($sitemapItemCollections as $collection) {
            foreach (self::writeXMLNew($baseUri, $uriPrefix, $dir, $collection) as $file) {
                $files[] = $file;
            }
        }

        self::writeIndex($dir, $indexFileName, $files);
    }

    /**
     * @param SitemapItem[] $items
     *
     * @throws SitemapFileException
     */
    private static function writeRealXML(
        string $baseUri,
        string $dir,
        string $fileName,
        int $lastUpdate,
        array $items,
    ): SitemapIndexItem {
        if (!$xmlArchive = gzopen($dir.$fileName.'.xml.gz', 'wb9')) {
            throw new SitemapFileException(\sprintf('Error opening outputfile [%s%s.xml.gz]', $dir, $fileName));
        }

        $urlXml = '';
        foreach ($items as $item) {
            $urlXml .= $item->asXml();
        }

        $xml = \sprintf(SitemapItem::getSitemapTemplate(), $urlXml);

        gzwrite($xmlArchive, $xml);
        gzclose($xmlArchive);

        return new SitemapIndexItem(
            \sprintf('%s%s.xml.gz', $baseUri, $fileName),
            $lastUpdate,
        );
    }

    /**
     * @throws SitemapFileException
     *
     * @return SitemapIndexItem[]
     *
     * @psalm-suppress RedundantCondition
     */
    private static function writeXMLNew(
        string $baseUri,
        string $uriPrefix,
        string $dir,
        SitemapItemCollection $collection,
    ): array {
        if (!$fileName = $collection->getFileName()) {
            return [];
        }

        $items = $collection->getItems();
        $lastUpdate = $collection->getLastMod() ?? time();

        if (\count($items) <= self::MAX_ITEMS_PER_FILE) {
            return self::writeItemsXML($baseUri, $uriPrefix, $dir, $fileName, $items, $lastUpdate);
        }

        $indexItems = [];
        foreach (self::splitByBeginLetter($items) as $letter => $items) {
            foreach (self::writeItemsXML($baseUri, $uriPrefix, $dir, $fileName.'_'.$letter, $items, $lastUpdate) as $indexItem) {
                $indexItems[] = $indexItem;
            }
        }

        return $indexItems;
    }

    /**
     * @param SitemapItem[] $items
     *
     * @throws SitemapFileException
     */
    private static function writeRealXMLNew(
        string $baseUri,
        string $uriPrefix,
        string $dir,
        string $fileName,
        int $lastUpdate,
        array $items,
    ): SitemapIndexItem {
        if (!$xmlArchive = gzopen($dir.$fileName.'.xml.gz', 'wb9')) {
            throw new SitemapFileException(\sprintf('Error opening outputfile [%s%s.xml.gz]', $dir, $fileName));
        }

        $urlXml = '';
        foreach ($items as $item) {
            $urlXml .= $item->asXml($baseUri);
        }

        $xml = \sprintf(SitemapItem::getSitemapTemplate(), $urlXml);

        gzwrite($xmlArchive, $xml);
        gzclose($xmlArchive);

        return new SitemapIndexItem(
            \sprintf('%s%s%s.xml.gz', $baseUri, $uriPrefix, $fileName),
            $lastUpdate,
        );
    }

    /**
     * @throws SitemapFileException
     *
     * @psalm-suppress RedundantCondition
     */
    private static function writeItemsXML(string $baseUri, string $uriPrefix, string $dir, string $fileName, array $items, int $lastUpdate): array
    {
        if (\count($items) === 0) {
            return [];
        }

        if (\count($items) <= self::MAX_ITEMS_PER_FILE) {
            return [self::writeRealXMLNew($baseUri, $uriPrefix, $dir, $fileName, $lastUpdate, $items)];
        }

        $indexes = [];
        $fileNumber = 1;
        while (\count($items)) {
            $itemsPart = array_splice($items, 0, self::MAX_ITEMS_PER_FILE);

            $indexes[] = self::writeRealXMLNew($baseUri, $uriPrefix, $dir, $fileName.($fileNumber > 1 ? '_'.$fileNumber : ''), $lastUpdate, $itemsPart);
            ++$fileNumber;
        }

        return $indexes;
    }

    /**
     * @param SitemapItem[] $items
     */
    private static function splitByBeginLetter(array $items): array
    {
        $byLetter = [];
        foreach ($items as $item) {
            $letter = mb_strtolower($item->getLoc());
            $letter = preg_replace('|^.*/([^/]+)/?$|', '$1', $letter);
            $letter = preg_replace('|[^a-z]|', '', $letter);
            $letter = mb_substr($letter, 0, 1);
            if (!$letter) {
                $letter = 'a';
            }

            $byLetter[$letter][] = $item;
        }

        return $byLetter;
    }
}
