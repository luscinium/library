<?php

declare(strict_types=1);

namespace Luscinium\Library\Sitemap\Exception;

class SitemapFileException extends \RuntimeException
{
}
