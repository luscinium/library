<?php

declare(strict_types=1);

namespace Luscinium\Library\IpApi;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Luscinium\Library\IpApi\Exception\IpApiException;

readonly class IpApi
{
    public function __construct(
        private ClientInterface $httpClient,
        private string $apiKey,
    ) {
    }

    /**
     * @throws IpApiException
     */
    public function getResult(string $ip): ?Result
    {
        $json = $this->getJson($ip);

        return $json
            ? Result::fromArray($json)
            : null;
    }

    /**
     * @throws IpApiException
     */
    private function getJson(string $ip): ?array
    {
        if (preg_match('/[^0-9.:A-F]/i', $ip)) {
            throw new IpApiException('Invalid IP address');
        }

        try {
            $response = $this->httpClient->request('GET', 'https://api.ipapi.is/?key='.$this->apiKey.'&q='.$ip);
        } catch (GuzzleException $e) {
            throw new IpApiException(\sprintf('Error requesting IP [%s]: %s', $ip, $e->getMessage()));
        }
        if ($response->getStatusCode() !== 200) {
            throw new IpApiException(\sprintf('Error requesting IP [%s]: %s', $ip, $response->getReasonPhrase()));
        }

        return json_decode($response->getBody()->getContents(), true);
    }
}
