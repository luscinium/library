<?php

declare(strict_types=1);

namespace Luscinium\Library\IpApi;

use JetBrains\PhpStorm\Pure;

readonly class Result
{
    private function __construct(
        private string $ip,
        private ?string $rir,
        private ?string $cidr,
        private bool $isBogon,
        private bool $isMobile,
        private false|string $isCrawler,
        private bool $isDatacenter,
        private bool $isTor,
        private bool $isProxy,
        private bool $isVpn,
        private bool $isAbuser,
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['ip'],
            $data['rir'],
            $data['asn']['route'] ?? $data['company']['network'] ?? null,
            (bool) $data['is_bogon'],
            (bool) $data['is_mobile'],
            $data['is_crawler'],
            (bool) $data['is_datacenter'],
            (bool) $data['is_tor'],
            (bool) $data['is_proxy'],
            (bool) $data['is_vpn'],
            (bool) $data['is_abuser'],
        );
    }

    #[Pure]
    public function getIp(): string
    {
        return $this->ip;
    }

    #[Pure]
    public function getRir(): ?string
    {
        return $this->rir;
    }

    #[Pure]
    public function getCidr(): ?string
    {
        return $this->cidr;
    }

    #[Pure]
    public function isBogon(): bool
    {
        return $this->isBogon;
    }

    #[Pure]
    public function isMobile(): bool
    {
        return $this->isMobile;
    }

    #[Pure]
    public function isCrawler(): false|string
    {
        return $this->isCrawler;
    }

    #[Pure]
    public function isDatacenter(): bool
    {
        return $this->isDatacenter;
    }

    #[Pure]
    public function isTor(): bool
    {
        return $this->isTor;
    }

    #[Pure]
    public function isProxy(): bool
    {
        return $this->isProxy;
    }

    #[Pure]
    public function isVpn(): bool
    {
        return $this->isVpn;
    }

    #[Pure]
    public function isAbuser(): bool
    {
        return $this->isAbuser;
    }

    #[Pure]
    public function isHome(): bool
    {
        return $this->isCrawler() === false
            && !$this->isDatacenter()
            && !$this->isTor()
            && !$this->isProxy()
            && !$this->isVpn()
            && !$this->isAbuser();
    }
}
