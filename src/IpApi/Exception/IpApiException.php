<?php

declare(strict_types=1);

namespace Luscinium\Library\IpApi\Exception;

class IpApiException extends \RuntimeException
{
}
