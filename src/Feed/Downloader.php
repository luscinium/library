<?php

declare(strict_types=1);

namespace Luscinium\Library\Feed;

use Luscinium\Library\Formatter;

class Downloader
{
    private function __construct()
    {
        throw new \LogicException("Can't touch this!");
    }

    /**
     * @throws FeedException
     */
    public static function downloadViaFtp(string $directory, string $gzFileName, string $ftpServer, string $ftpUser, string $ftpPassword): void
    {
        if (!$fh = fopen($directory.\DIRECTORY_SEPARATOR.$gzFileName, 'w')) {
            throw new FeedException(\sprintf('File "%s" can not be written: %s', $gzFileName, Formatter::print_r(error_get_last())));
        }

        $ch = curl_init();
        curl_setopt($ch, \CURLOPT_FILE, $fh);
        curl_setopt($ch, \CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, \CURLOPT_FTP_USE_EPSV, true);
        curl_setopt($ch, \CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, \CURLOPT_URL, \sprintf('ftp://%s/%s', $ftpServer, $gzFileName));
        curl_setopt($ch, \CURLOPT_USERPWD, $ftpUser.':'.$ftpPassword);

        if (curl_exec($ch) === false) {
            throw new FeedException(\sprintf('File "%s" not downloaded: %s', $gzFileName, curl_error($ch)));
        }

        fclose($fh);
        curl_close($ch);
    }

    /**
     * @throws FeedException
     */
    public static function downloadViaHttp(string $directory, string $url, string $fileName): void
    {
        if (!$fh = fopen($directory.\DIRECTORY_SEPARATOR.$fileName, 'w')) {
            throw new FeedException(\sprintf('File "%s" can not be written: %s', $fileName, Formatter::print_r(error_get_last())));
        }

        $ch = curl_init();
        curl_setopt($ch, \CURLOPT_FILE, $fh);
        curl_setopt($ch, \CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, \CURLOPT_URL, $url);

        if (curl_exec($ch) === false) {
            throw new FeedException(\sprintf('File "%s" not downloaded: %s', $fileName, curl_error($ch)));
        }

        fclose($fh);
        curl_close($ch);
    }
}
