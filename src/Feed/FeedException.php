<?php

declare(strict_types=1);

namespace Luscinium\Library\Feed;

class FeedException extends \Exception
{
}
