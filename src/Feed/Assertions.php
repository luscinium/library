<?php

declare(strict_types=1);

namespace Luscinium\Library\Feed;

use Luscinium\Library\Formatter;

class Assertions
{
    /**
     * @throws FeedException
     */
    public static function assertFileExistsAndIsReadable(string $fileName): void
    {
        if (!file_exists($fileName)) {
            throw new FeedException(\sprintf('File not found: %s', $fileName));
        }

        if (!is_readable($fileName)) {
            throw new FeedException(\sprintf('File "%s" can not be opened: %s', $fileName, Formatter::print_r(error_get_last())));
        }
    }
}
