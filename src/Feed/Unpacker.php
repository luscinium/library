<?php

declare(strict_types=1);

namespace Luscinium\Library\Feed;

use Luscinium\Library\Formatter;

class Unpacker
{
    private function __construct()
    {
        throw new \LogicException("Can't touch this!");
    }

    /**
     * @throws FeedException
     */
    public static function unpack(string $directory, string $gzFileName, string $fileName): void
    {
        if (!$gzHandle = gzopen($directory.\DIRECTORY_SEPARATOR.$gzFileName, 'rb')) {
            throw new FeedException(\sprintf('File "%s" can not be opened: %s', $gzFileName, Formatter::print_r(error_get_last())));
        }

        if (!$fileHandle = fopen($directory.\DIRECTORY_SEPARATOR.$fileName, 'w')) {
            throw new FeedException(\sprintf('File "%s" can not be written: %s', $fileName, Formatter::print_r(error_get_last())));
        }

        while (!gzeof($gzHandle)) {
            $string = gzread($gzHandle, 4096);
            fwrite($fileHandle, $string, \strlen($string));
        }

        gzclose($gzHandle);
        fclose($fileHandle);

        if (!unlink($directory.\DIRECTORY_SEPARATOR.$gzFileName)) {
            throw new FeedException(\sprintf('File "%s" is not deleted after unpacking: %s', $gzFileName, Formatter::print_r(error_get_last())));
        }
    }
}
