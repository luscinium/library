<?php

declare(strict_types=1);

namespace Luscinium\Library\Feed;

class Preparer
{
    /**
     * @throws FeedException
     */
    public static function prepare(string $destination): void
    {
        if (!file_exists($destination) && !mkdir($destination) && !is_dir($destination)) {
            throw new FeedException(\sprintf('Directory "%s" was not created', $destination));
        }
    }
}
