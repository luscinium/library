<?php

declare(strict_types=1);

namespace Luscinium\Library\Http;

interface ProxyInterface
{
    public function get(): string;

    public function delete(string $proxy): void;
}
