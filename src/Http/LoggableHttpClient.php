<?php

declare(strict_types=1);

namespace Luscinium\Library\Http;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use JetBrains\PhpStorm\NoReturn;
use Luscinium\Library\Http\Exception\HttpClientException;
use Luscinium\Library\Http\Exception\HttpException;
use Psr\Log\LoggerInterface;

class LoggableHttpClient
{
    private const MAX_ATTEMPTS = 10;
    private ?string $baseUri = null;
    private ?string $previous = null;
    private array $headers = [];
    private CookieJar $jar;

    public function __construct(
        private readonly ?LoggerInterface $logger,
        private ClientInterface $httpClient,
        private readonly ?ProxyInterface $proxyService = null,
    ) {
        $this->jar = new CookieJar();
    }

    public static function fromLogger(?LoggerInterface $logger = null, ?ProxyInterface $proxyService = null): self
    {
        return new self(
            $logger,
            self::getHttpClient(),
            $proxyService,
        );
    }

    public function setBaseUri(string $baseUri): void
    {
        $this->baseUri = $baseUri;
        $this->httpClient = self::getHttpClient($this->baseUri, $this->headers);
    }

    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
        $this->httpClient = self::getHttpClient($this->baseUri, $this->headers);
    }

    /**
     * @throws HttpException
     */
    public function get(string $url, array $params = []): string
    {
        return $this->do('GET', $url, $params);
    }

    /**
     * @throws HttpException
     */
    public function post(string $url, array $params = []): string
    {
        return $this->do('POST', $url, $params);
    }

    /**
     * @throws HttpException
     */
    private function do(string $method, string $url, array $params): string
    {
        $debugMessage = \sprintf('%s %s', $method, $url);
        if ($params) {
            $debugMessage .= \sprintf(' [%s]', http_build_query($params));
        }
        $this->printDebug($debugMessage);
        if (!isset($params['headers']['Referer'])) {
            $params['headers']['Referer'] = $this->previous ?? $url;
        }
        $this->previous = $url;

        return $this->doRequest($method, $url, $params);
    }

    private static function getHttpClient(?string $baseUri = null, array $headers = []): Client
    {
        $defaultHeaders = [
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/png,image/svg+xml,*/*;q=0.8',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Accept-Language' => 'nl,en-US;q=0.7,en;q=0.3',
            'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:135.0) Gecko/20100101 Firefox/135.0',
        ];

        return new Client([
            'base_uri' => $baseUri,
            'version' => 2.0,
            'headers' => array_merge($defaultHeaders, $headers),
            'timeout' => 10,
            'verify' => false,
            'allow_redirects' => ['max' => self::MAX_ATTEMPTS],
        ]);
    }

    /**
     * @throws HttpException
     */
    private function doRequest(string $method, string $url, array $params, int $attempt = 1): string
    {
        $this->addProxy($attempt, $params);
        $params['cookies'] = $this->jar;

        try {
            $response = $this->httpClient->request($method, $url, $params);

            $contents = $response->getBody()->getContents();

            if ($this->hasProtection($contents)) {
                $this->printDebug(\sprintf('Retry for protection at %s', $url));

                if ($attempt > self::MAX_ATTEMPTS) {
                    return $this->doRequest($method, $url, $params, ++$attempt);
                }
            }

            return $contents;
        } catch (GuzzleException $e) {
            $this->printDebug(\sprintf('%s @ %s %s: %s', $e::class, $method, $url, $e->getMessage()));

            /**
             * @psalm-suppress RedundantCast
             */
            $code = (int) $e->getCode();
            if ($this->needsRetry($e, $code)) {
                if ($code === 429) {
                    $sleep = 30;
                    $this->printDebug(\sprintf('Sleeping %d seconds for `429 Too Many Requests` at %s', $sleep, $url));
                    sleep($sleep);
                } else {
                    $this->printDebug(\sprintf('Retry %s', $url));
                }

                if ($attempt > self::MAX_ATTEMPTS) {
                    return $this->doRequest($method, $url, $params, ++$attempt);
                }
            }

            if ($code === 0) {
                $this->throwError(\sprintf('%s @ %s %s: %s', $e::class, $method, $url, $e->getMessage()), $e);
            } elseif ($code === 404) {
                $this->throwError(\sprintf('HTTP error %3d @ %s %s', $code, $method, $url), $e);
            } else {
                $this->throwError(\sprintf('HTTP error %3d @ %s %s: %s', $code, $method, $url, $e->getMessage()), $e);
            }
        }
    }

    private function addProxy(int $attempt, array &$params): void
    {
        if ($this->proxyService === null) {
            return;
        }

        if (\array_key_exists('proxy', $params)) {
            $this->proxyService->delete($params['proxy']);
        }

        if ($attempt > 1) {
            $params['proxy'] = $this->proxyService->get();
        }
    }

    private function needsRetry(\Throwable $e, int $code): bool
    {
        return $e instanceof ConnectException || $code === 403 || $code === 429 || self::isHTTP2Error($e);
    }

    /**
     * See https://curl.se/libcurl/c/libcurl-errors.html#CURLEHTTP2.
     */
    private static function isHTTP2Error(\Throwable $e): bool
    {
        if (!$e instanceof RequestException) {
            return false;
        }

        return str_contains($e->getMessage(), 'cURL error 16');
    }

    private function hasProtection(string $contents): bool
    {
        return (bool) preg_match('#<title>StackPath</title>#i', $contents);
    }

    private function printDebug(string $debugMessage): void
    {
        $this->logger?->debug($debugMessage);
    }

    /**
     * @throws HttpException
     */
    #[NoReturn]
    private function throwError(string $errorMessage, ?\Throwable $previous = null): void
    {
        if ($previous instanceof ClientException) {
            throw new HttpClientException($errorMessage, previous: $previous);
        }
        throw new HttpException($errorMessage, previous: $previous);
    }
}
