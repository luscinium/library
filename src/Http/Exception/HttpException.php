<?php

declare(strict_types=1);

namespace Luscinium\Library\Http\Exception;

class HttpException extends \RuntimeException
{
}
