<?php

declare(strict_types=1);

namespace Luscinium\Library;

class Formatter
{
    private function __construct()
    {
        throw new \LogicException("Can't touch this!");
    }

    /**
     * Returns human-readable representation of a variable.
     */
    public static function print_r(?array $object): string
    {
        try {
            $rv = var_export($object, true);
            $rv = preg_replace('/\s+/', ' ', $rv);
            $rv = preg_replace('/,\s*\)/', ')', $rv);

            return preg_replace('/array\s*\(\s*/', 'Array(', $rv);
        } catch (\Throwable) {
        }

        return '';
    }
}
