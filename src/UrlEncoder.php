<?php

declare(strict_types=1);

namespace Luscinium\Library;

use JetBrains\PhpStorm\Pure;

class UrlEncoder
{
    private const FIELDS = [
        'linkId',
        'origin',
        'keyword',
        'sub',
        'subid',
        'url',
    ];
    private const SEPARATOR = '||';
    private const UNSAFE_CHARS = ['+', '/'];
    private const SAFE_CHARS = ['-', '_'];

    private function __construct()
    {
        throw new \LogicException("Can't touch this!");
    }

    #[Pure]
    public static function encode(array $data): string
    {
        $string = '';
        foreach (self::FIELDS as $field) {
            if ($string) {
                $string .= self::SEPARATOR;
            }
            $string .= ($data[$field] ?? '');
        }

        return self::urlSafeEncode($string);
    }

    /**
     * @return string[]
     *
     * @psalm-return array<string>
     */
    #[Pure]
    public static function decode(string $url): array
    {
        $string = self::urlSafeDecode($url);

        $data = [];
        foreach (explode(self::SEPARATOR, $string) as $item) {
            $count = \count($data);
            if (isset(self::FIELDS[$count])) {
                $data[self::FIELDS[$count]] = $item;
            }
        }

        return $data;
    }

    /**
     * @psalm-suppress ImpureFunctionCall
     */
    #[Pure]
    public static function addTracking(string $url, array $data): string
    {
        $trackingString = self::getTrackingString($data);

        $destinationData = parse_url($url);
        parse_str($destinationData['query'] ?? '', $destinationParams);
        if (!self::fillTrackingParam($destinationParams, $trackingString)) {
            $destinationParams['ws'] = $trackingString;
        }
        $destinationData['query'] = http_build_query($destinationParams);

        return self::unParseUrl($destinationData);
    }

    #[Pure]
    private static function urlSafeEncode(string $string): string
    {
        $data = base64_encode($string);
        $data = str_replace(self::UNSAFE_CHARS, self::SAFE_CHARS, $data);

        return str_replace('=', '', $data);
    }

    #[Pure]
    private static function urlSafeDecode(string $string): string
    {
        $data = str_replace(self::SAFE_CHARS, self::UNSAFE_CHARS, $string);
        $mod4 = \strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }

        return base64_decode($data);
    }

    #[Pure]
    private static function getTrackingString(array $data): string
    {
        $string = '';
        foreach (self::FIELDS as $field) {
            if ($field === 'url') {
                continue;
            }
            if ($string) {
                $string .= self::SEPARATOR;
            }
            $string .= ($data[$field] ?? '');
        }

        return $string;
    }

    #[Pure]
    private static function fillTrackingParam(array &$destinationParams, string $trackingString): bool
    {
        foreach (['ws', '4dsid', 'sid', 'subid', 'origin'] as $param) {
            if (\array_key_exists($param, $destinationParams)) {
                $destinationParams[$param] = $trackingString;

                return true;
            }
        }

        return false;
    }

    #[Pure]
    private static function unParseUrl(array $data): string
    {
        $scheme = isset($data['scheme']) ? $data['scheme'].'://' : '';
        $host = $data['host'] ?? '';
        $port = isset($data['port']) ? ':'.$data['port'] : '';
        $user = $data['user'] ?? '';
        $pass = isset($data['pass']) ? ':'.$data['pass'] : '';
        $pass = ($user || $pass) ? "$pass@" : '';
        $path = $data['path'] ?? '';
        $query = isset($data['query']) ? '?'.$data['query'] : '';
        $fragment = isset($data['fragment']) ? '#'.$data['fragment'] : '';

        return \sprintf('%s%s%s%s%s%s%s%s',
            $scheme,
            $user,
            $pass,
            $host,
            $port,
            $path,
            $query,
            $fragment
        );
    }
}
