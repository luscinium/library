<?php

declare(strict_types=1);

namespace Luscinium\Library;

class AssetHasher
{
    private function __construct()
    {
        throw new \LogicException("Can't touch this!");
    }

    public static function getVersionFromFiles(string $baseDir): string
    {
        $hash = '';
        foreach (glob($baseDir.'/public/css/*.css') as $cssFileName) {
            $hash .= md5_file($cssFileName);
        }
        foreach (glob($baseDir.'/public/js/*.js') as $jsFileName) {
            $hash .= md5_file($jsFileName);
        }

        return md5($hash);
    }
}
