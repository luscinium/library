<?php

declare(strict_types=1);

namespace Luscinium\Library\ValueObject;

readonly class FaqItem
{
    public function __construct(
        private string $question,
        private string $answer,
    ) {
    }

    public function getQuestion(): string
    {
        return $this->question;
    }

    public function getAnswer(): string
    {
        return $this->answer;
    }
}
