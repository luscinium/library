<?php

declare(strict_types=1);

namespace Luscinium\Library\ValueObject\PageElement;

readonly class BreadcrumbItem
{
    public function __construct(
        private string $name,
        private string $url,
        private ?string $imageUrl = null,
        private ?string $extraText = null,
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function getExtraText(): ?string
    {
        return $this->extraText;
    }
}
