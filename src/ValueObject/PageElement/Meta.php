<?php

declare(strict_types=1);

namespace Luscinium\Library\ValueObject\PageElement;

use JetBrains\PhpStorm\Pure;

class Meta
{
    public function __construct(
        private string $title = '',
        private string $description = '',
        private string $robots = '',
    ) {
        $this->setTitle($title);
        $this->setDescription($description);
        $this->setRobots($robots);
    }

    #[Pure]
    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    #[Pure]
    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    #[Pure]
    public function getRobots(): string
    {
        return $this->robots;
    }

    public function setRobots(string $robots): void
    {
        $this->robots = $robots;
    }

    public function setTitleWithReplace(string $title, array $vars): void
    {
        $this->setTitle(self::replaceVars($title, $vars));
    }

    public function setDescriptionWithReplace(string $description, array $vars): void
    {
        $this->setDescription(self::replaceVars($description, $vars));
    }

    #[Pure]
    private static function replaceVars(string $string, array $vars): string
    {
        foreach ($vars as $key => $value) {
            if (!\is_string($value)) {
                continue;
            }

            $string = str_replace('{'.$key.'}', $value, $string);
        }

        return $string;
    }
}
