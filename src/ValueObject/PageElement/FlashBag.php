<?php

declare(strict_types=1);

namespace Luscinium\Library\ValueObject\PageElement;

use Illuminate\Contracts\Session\Session;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class FlashBag
{
    private const SUCCESS = 'success';
    private const NOTICE = 'notice';
    private const WARN = 'warn';
    private const ERROR = 'error';

    public function __construct(
        private readonly ?FlashBagInterface $flashBag = null,
        private readonly ?Session $session = null,
    ) {
    }

    public function addSuccessFlash(string $message): void
    {
        $this->addFlash(self::SUCCESS, $message);
    }

    public function addNoticeFlash(string $message): void
    {
        $this->addFlash(self::NOTICE, $message);
    }

    public function addWarningFlash(string $message): void
    {
        $this->addFlash(self::WARN, $message);
    }

    public function addErrorFlash(string $message): void
    {
        $this->addFlash(self::ERROR, $message);
    }

    public function getNumberOfFlashes(): int
    {
        try {
            $messages = $this->asArray(false);

            return \count($messages[self::SUCCESS] ?? [])
                + \count($messages[self::NOTICE] ?? [])
                + \count($messages[self::WARN] ?? [])
                + \count($messages[self::ERROR] ?? []);
        } catch (\Throwable) {
        }

        return 0;
    }

    public function asArray(bool $clear = true): array
    {
        if ($this->flashBag) {
            $all = $this->flashBag->peekAll();
            if ($clear) {
                $this->flashBag->clear();
            }

            return $all;
        }

        if ($this->session) {
            $all = [
                self::SUCCESS => $this->session->get(self::SUCCESS),
                self::NOTICE => $this->session->get(self::NOTICE),
                self::WARN => $this->session->get(self::WARN),
                self::ERROR => $this->session->get(self::ERROR),
            ];

            if ($clear) {
                $this->session->forget(self::SUCCESS);
                $this->session->forget(self::NOTICE);
                $this->session->forget(self::WARN);
                $this->session->forget(self::ERROR);
            }

            return $all;
        }

        return [];
    }

    private function addFlash(string $type, string $message): void
    {
        $this->flashBag?->add($type, $message);

        if ($this->session) {
            $existing = $this->session->get($type);
            if (!$existing) {
                $existing = [];
            } elseif (!\is_array($existing)) {
                $existing = [$existing];
            }
            $existing[] = $message;
            if (method_exists($this->session, 'flash')) {
                $this->session->flash($type, $existing);
            } else {
                $this->session->put($type, $existing);
            }
        }
    }
}
