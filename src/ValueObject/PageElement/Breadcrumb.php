<?php

declare(strict_types=1);

namespace Luscinium\Library\ValueObject\PageElement;

class Breadcrumb
{
    /**
     * @var BreadcrumbItem[]
     */
    private array $breadcrumbItems = [];

    public function __construct(
        private readonly string $baseUri,
    ) {
    }

    public function addBreadcrumbItem(?BreadcrumbItem $breadcrumbItem): void
    {
        if ($breadcrumbItem !== null) {
            $this->breadcrumbItems[] = $breadcrumbItem;
        }
    }

    public function getBaseUri(): string
    {
        return $this->baseUri;
    }

    /**
     * @return BreadcrumbItem[]
     */
    public function getBreadcrumbItems(): array
    {
        return $this->breadcrumbItems;
    }
}
